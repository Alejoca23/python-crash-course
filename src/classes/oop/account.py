class Account:
    def __init__(self, owner, balance):
        self.owner: str = owner
        self.balance: float = balance

    def __repr__(self):
        return f'Account({self.owner!r}, {self.balance!r})'

    def deposit(self, amount):
        self.balance += amount

    def withdraw(self, amount):
        self.deposit(-amount)

    def inquiry(self):
        return self.balance
