from account import Account
from account_portfolio import AccountPortfolio


def account_test():
    account: Account = Account('Kalenji', 565622.56)
    print(account)
    account.deposit(100000)
    account.withdraw(7777)
    print(f'Inquiry: {account.inquiry()!r}')
    print(account)


def portfolio_test():
    port = AccountPortfolio()
    port.add_account(Account('Guido', 1000.0))
    port.add_account(Account('Eva', 50.0))

    print(port.total_funds())
    print(len(port))

    # Print the accounts
    for account in port:
        print(account)

    # Access an individual account by index
    print(port[1].inquiry())
    # print(port.accounts[1].inquiry())


if __name__ == '__main__':
    # account_test()
    portfolio_test()
