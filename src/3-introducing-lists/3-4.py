guests_list: list = ['Margaret Hamilton', 'Isaac Asimov', 'Richard Feynman', 'Ada Lovelace', 'Carl Sagan']

for guest in guests_list:
    print(f"Hi {guest}, I'd like to invite you to dinner and have a little chat.")
