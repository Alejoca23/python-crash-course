_places_to_visit: list = ['Villa de Leyva', 'Oulu', 'Saariselkä', 'Belfast', 'Nankin', 'Chichén Itzá', 'Barcelona']
print(f'Original: {_places_to_visit}')

print(f'Using sorted() function: {sorted(_places_to_visit)}')
print(f'Original: {_places_to_visit}')
_places_to_visit.reverse()
print(f'Using reverse() function: {_places_to_visit}')
_places_to_visit.reverse()
print(f'Using reverse() function again: {_places_to_visit}')
_places_to_visit.sort()
print(f'Using sort() function: {_places_to_visit}')
_places_to_visit.sort(reverse=True)
print(f'Using sort() function again with reverse argument: {_places_to_visit}')
