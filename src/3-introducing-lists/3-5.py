from random import randrange

guests_list: list = ['Margaret Hamilton', 'Isaac Asimov', 'Richard Feynman', 'Ada Lovelace', 'Carl Sagan']

for guest in guests_list:
    print(f"Hi {guest}, I'd like to invite you to dinner and have a little chat.")

print('-------------------------')
decline: int = randrange(len(guests_list))
print(f"Ohh!, {guests_list[decline]} can't go there.")
guests_list[decline] = 'Leonardo Da Vinci'

for guest in guests_list:
    print(f"{guest}, confirmed the dinner.")
