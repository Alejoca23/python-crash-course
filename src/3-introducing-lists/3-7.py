from random import randrange

guests_list: list = ['Margaret Hamilton', 'Isaac Asimov', 'Richard Feynman', 'Ada Lovelace', 'Carl Sagan']

for guest in guests_list:
    print(f"Hi {guest}, I'd like to invite you to dinner and have a little chat.")

print('--------------------------')
decline: int = randrange(len(guests_list))
print(f"Ohh!, {guests_list[decline]} can't come to dinner.")
guests_list[decline] = 'Leonardo Da Vinci'

for guest in guests_list:
    print(f"{guest}, confirmed the dinner.")
print('--------------------------')
print('Heyy, I found a bigger table for dinner.')
# New guests
guests_list.insert(2, 'Hedy Lamarr')
guests_list.insert(0, 'Nicolás Copérnico')
guests_list.append('Isaac Newton')

# Just two guests
print(guests_list)
print('Sorry, the big table won\'t arrive in time, I just can invite two people for dinner :(')

for i in range(len(guests_list)):
    random_number = randrange(0, len(guests_list))
    if len(guests_list) == 2:
        break
    bye_guest = guests_list.pop(random_number)
    print(f'{bye_guest}, sorry, I hope you can come next time!')

print(guests_list)

for guest in guests_list:
    print(f'You still are invited {guest}')

# del guests_list[0]
# del guests_list[1]
del guests_list[:]

print(f'Ohhh noo!!!\nNow I have an empty list: {guests_list}')
