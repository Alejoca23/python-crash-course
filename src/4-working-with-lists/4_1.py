pizzas: [str] = ['Pepperoni', 'Chicken and mushrooms', 'Bacon', 'Extra cheese']

for pizza in pizzas:
    print(f'I like {pizza} pizza')

print(f'The first kind of pizza I ate {pizzas[0]}')
print(f'The last kind of pizza I ate {pizzas[1]}')
print(f'I really love pizza')
